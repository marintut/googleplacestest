package com.example.googleplacesapptest.features.place_details;

import android.support.annotation.StringRes;
import com.example.googleplacesapptest.network.api_model.Place;
import java.util.ArrayList;

public interface PlacesMapView
{

    void presentPlaces(ArrayList<Place> places);
    void showSnackBarMessage(String message);
    void showSnackBarMessage(@StringRes int messageResourceId);
    void showSearchLoading(boolean show);
}
