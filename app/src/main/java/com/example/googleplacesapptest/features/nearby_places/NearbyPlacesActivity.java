package com.example.googleplacesapptest.features.nearby_places;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.example.googleplacesapptest.R;
import com.example.googleplacesapptest.features.place_details.PlaceDetailsActivity;
import com.example.googleplacesapptest.features.place_details.PlacesMapPresenter;
import com.example.googleplacesapptest.features.place_details.PlacesMapView;
import com.example.googleplacesapptest.network.api_model.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import io.reactivex.disposables.Disposable;

public class NearbyPlacesActivity extends AppCompatActivity implements PlacesMapView,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener
{
    private final int PERMISSION_GET_LOCATION = 1000;

    private EditText searchBox;
    private MapView mapView;
    private ProgressBar searchProgressBar;
    private Button searchButton;

    private GoogleMap mGoogleMap;
    private PlacesMapPresenter presenter;
    private ArrayList<Place> places = new ArrayList<>();
    private Disposable searchDisposable = null;
    private int lastMapDragReason = 0;

    /**
     * Lifecycle callbacks
     * */

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_map);

        presenter = new PlacesMapPresenter(this);

        searchButton = findViewById(R.id.search_btn);
        mapView = findViewById(R.id.map_view);
        searchBox = findViewById(R.id.search_box_et);
        searchProgressBar = findViewById(R.id.progress_bar);

        searchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                searchPlaces();
            }
        });

        prepareMap(savedInstanceState);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (mapView != null)
            mapView.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (mapView != null)
            mapView.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        if (searchDisposable != null && !searchDisposable.isDisposed())
            searchDisposable.dispose();
    }

    /**
     * Search for places
     * */

    public void searchPlaces()
    {
        LatLng mapCenterCoordinates = mGoogleMap.getCameraPosition().target;
        String searchText = searchBox.getText().toString();

        searchDisposable = presenter.searchPlaces(searchText, mapCenterCoordinates);
    }

    /**
     * Google maps
     **/

    @SuppressLint("MissingPermission")
    public void prepareMap(Bundle savedInstanceState)
    {
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap googleMap)
            {
                mGoogleMap = googleMap;

                mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
                mGoogleMap.setOnInfoWindowClickListener(NearbyPlacesActivity.this);
                mGoogleMap.setOnCameraMoveStartedListener(NearbyPlacesActivity.this);
                mGoogleMap.setOnCameraIdleListener(NearbyPlacesActivity.this);

                if (hasLocationPermission())
                {
                    mGoogleMap.setMyLocationEnabled(true);
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

                    zoomToCurrentLocation();
                }
                else
                {
                    askUserForLocation();
                }

            }
        });
    }


    @Override
    public void onCameraIdle()
    {
        //Check if last map move was `dragging` by the user, and make a request to get new near places
        if (lastMapDragReason == REASON_GESTURE)
        {
            LatLng mapCenterCoordonates = mGoogleMap.getCameraPosition().target;
            String searchText = searchBox.getText().toString();

            presenter.searchPlaces(searchText, mapCenterCoordonates);
        }
    }

    @Override
    public void onCameraMoveStarted(int reason)
    {
        lastMapDragReason = reason;
    }

    //Redraws all places markers on map
    private void updateMapMarkers()
    {
        if (mGoogleMap == null) return;

        mGoogleMap.clear();

        for (Place place: places)
        {
            LatLng placeLocation = new LatLng(place.geometry.location.lat,place.geometry.location.lng);

            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(placeLocation)
                    .title(place.name));

            marker.setTag(place.id);

            fakeId = place.id;
        }
    }

    @SuppressLint("MissingPermission")
    private void zoomToCurrentLocation()
    {
        if (!hasLocationPermission())
        {
            return;
        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));

        if (location != null)
        {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(17)
                    .build();

            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            searchPlaces();
        }
    }

    //Present to user the need of localization
    private void askUserForLocation()
    {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.nearby_places_request_location_persmission_title))
                .setMessage(getString(R.string.nearby_places_request_location_persmission_message))
                .setNegativeButton(android.R.string.cancel,null)
                .setPositiveButton(getString(R.string.app_general_continue_title), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        requestLocationPermission();
                    }
                })
                .show();
    }

    /**
     *   Runtime permissions management
     * */

    private void requestLocationPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

            requestPermissions(permissions, PERMISSION_GET_LOCATION);
        }
    }

    // Returns true if user has location permissions granted
    private boolean hasLocationPermission()
    {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_GET_LOCATION)
        {
            if (hasLocationPermission() && mGoogleMap != null)
            {
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

                zoomToCurrentLocation();
            }
        }
    }

    /********/

    String fakeId = null;

    //Called when
    @Override
    public void presentPlaces(ArrayList<Place> places)
    {
        this.places = places;

        updateMapMarkers();
    }

    @Override
    public void showSnackBarMessage(String message)
    {
        View parentLayout = findViewById(android.R.id.content);

        if (parentLayout == null) return;

        Snackbar snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

    @Override
    public void showSnackBarMessage(int messageResourceId)
    {
        showSnackBarMessage(getString(messageResourceId));
    }

    @Override
    public void showSearchLoading(boolean show)
    {
        if (searchProgressBar == null || searchButton == null) return;

        if (show)
        {
            searchButton.setVisibility(View.INVISIBLE);
            searchProgressBar.setVisibility(View.VISIBLE);
        }
        else
        {
            searchProgressBar.setVisibility(View.INVISIBLE);
            searchButton.setVisibility(View.VISIBLE);
        }
    }

    //Handles Markers InfoWindow click event
    @Override
    public void onInfoWindowClick(Marker marker)
    {
        //String placeId = (String) marker.getTag();
        String placeId = fakeId;

        if (!TextUtils.isEmpty(placeId))
        {
            Place place = getPlaceById(placeId);

            if (place != null)
            {
                navigateToPlaceDetails(place);
            }
        }
    }

    //Returns the Place by id, or null, if there is no place with the same id
    @Nullable
    private Place getPlaceById(String id) {
        Place place = null;

        for (Place p : places) {
            if (p.id != null && p.id.equals(id)) {
                place = p;
                break;
            }
        }

        return place;
    }

    //Shows place details Activity
    private void navigateToPlaceDetails(Place place)
    {
        PlaceDetailsActivity.start(this, place);
    }
}
