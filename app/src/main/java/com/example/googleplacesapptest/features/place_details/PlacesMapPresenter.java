package com.example.googleplacesapptest.features.place_details;

import com.example.googleplacesapptest.C;
import com.example.googleplacesapptest.network.PlaceAPI;
import com.example.googleplacesapptest.network.RetrofitBuilder;
import com.example.googleplacesapptest.network.api_model.SearchPlacesResult;
import com.google.android.gms.maps.model.LatLng;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PlacesMapPresenter
{
    private PlacesMapView view;
    private PlaceAPI placeAPI;

    public PlacesMapPresenter(PlacesMapView view)
    {
        this.view = view;

        placeAPI = RetrofitBuilder.getInstance();
    }

    public Disposable searchPlaces(final String placeName, LatLng latLng)
    {
        final String location = latLng.latitude + "," + latLng.longitude;
        final int radius = 500;

        view.showSearchLoading(true);

        return placeAPI.searchPlaces(location,radius,placeName, C.GOOGLE_PLACES_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<SearchPlacesResult>()
                {
                    @Override
                    public void accept(SearchPlacesResult searchPlacesResult)
                    {
                        if (view == null) return;

                        view.showSearchLoading(false);
                        view.presentPlaces(searchPlacesResult.getResults());
                    }
                }, new Consumer<Throwable>()
                {
                    @Override
                    public void accept(Throwable throwable)
                    {

                        view.showSearchLoading(false);

                        if (view != null)
                        {
                            view.showSnackBarMessage(throwable.getMessage());
                        }
                    }
                });


//                        Gson gson = new Gson();
//
//                        SearchPlacesResult result = gson.fromJson(places,SearchPlacesResult.class);
//
//                        view.presentPlaces(result.getResults());
    }

}
