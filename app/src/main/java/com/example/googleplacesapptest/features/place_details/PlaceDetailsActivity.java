package com.example.googleplacesapptest.features.place_details;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.googleplacesapptest.R;
import com.example.googleplacesapptest.databinding.ActivityPlaceDetailsBinding;
import com.example.googleplacesapptest.network.api_model.Place;

public class PlaceDetailsActivity extends AppCompatActivity {

    private final static String EXTRAS_PLACE_ID = "EXTRAS_PLACE_ID";

    private ActivityPlaceDetailsBinding binding;

    private Place place= null;

    private ImageView placeIcon;

    public static void start(Context context, Place place)
    {
        Intent intent = new Intent(context, PlaceDetailsActivity.class);

        intent.putExtra(EXTRAS_PLACE_ID, place);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_place_details);

        placeIcon = findViewById(R.id.place_icon_iv);

        if (getIntent().getExtras() != null)
        {
            place = getIntent().getExtras().getParcelable(EXTRAS_PLACE_ID);
        }

        setUpToolbar();
        prepareView();
    }

    private void setUpToolbar()
    {
        ActionBar toolbar = getSupportActionBar();

        if (toolbar == null) return;

        toolbar.setDisplayHomeAsUpEnabled(true);
    }

    private void prepareView()
    {
        if (place == null) return;

        binding.setPlace(place);

        Glide.with(this).load(place.icon).into(placeIcon);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            onBackPressed();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
