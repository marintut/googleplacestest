package com.example.googleplacesapptest.network;

import com.example.googleplacesapptest.BuildConfig;
import com.example.googleplacesapptest.C;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marin Tutuc
 * marintutucv@gmail.com
 * on 11/10/17.
 */

public class RetrofitBuilder
{
    private static PlaceAPI placesApi;

    private static void setupRestClient()
    {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.Builder okHttpClient;

        okHttpClient = new OkHttpClient.Builder();

        okHttpClient
            .readTimeout(120, TimeUnit.SECONDS)
            .connectTimeout(120, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .retryOnConnectionFailure(false)
            .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(C.GOOGLE_PLACES_BASE_URL)
                .client(okHttpClient.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        placesApi = retrofit.create(PlaceAPI.class);
    }

    public static PlaceAPI newInstance()
    {
        setupRestClient();

        return placesApi;
    }

    public static PlaceAPI getInstance()
    {
        if (placesApi == null)
            newInstance();

        return placesApi;
    }
}
