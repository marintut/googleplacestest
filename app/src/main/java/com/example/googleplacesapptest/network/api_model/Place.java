package com.example.googleplacesapptest.network.api_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Place implements Parcelable
{
    @SerializedName("geometry")
    @Expose
    public Geometry geometry;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("opening_hours")
    @Expose
    public OpeningHours openingHours;
    @SerializedName("photos")
    @Expose
    public ArrayList<PlacePhoto> photos = null;
    @SerializedName("place_id")
    @Expose
    public String placeId;
    @SerializedName("plus_code")
    @Expose
    public PlusCode plusCode;
    @SerializedName("price_level")
    @Expose
    public int priceLevel;
    @SerializedName("rating")
    @Expose
    public float rating;
    @SerializedName("reference")
    @Expose
    public String reference;
    @SerializedName("scope")
    @Expose
    public String scope;
    @SerializedName("types")
    @Expose
    public ArrayList<String> types = null;
    @SerializedName("user_ratings_total")
    @Expose
    public int userRatingsTotal;
    @SerializedName("vicinity")
    @Expose
    public String vicinity;

    public Place() {
    }

    protected Place(Parcel in) {
        icon = in.readString();
        id = in.readString();
        name = in.readString();
        placeId = in.readString();
        priceLevel = in.readInt();
        rating = in.readFloat();
        reference = in.readString();
        scope = in.readString();
        types = in.createStringArrayList();
        userRatingsTotal = in.readInt();
        vicinity = in.readString();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(icon);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(placeId);
        dest.writeInt(priceLevel);
        dest.writeFloat(rating);
        dest.writeString(reference);
        dest.writeString(scope);
        dest.writeStringList(types);
        dest.writeInt(userRatingsTotal);
        dest.writeString(vicinity);
    }
}
