package com.example.googleplacesapptest.network.api_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PlacePhoto
{
    @SerializedName("height")
    @Expose
    public int height;
    @SerializedName("html_attributions")
    @Expose
    public ArrayList<String> htmlAttributions = null;
    @SerializedName("photo_reference")
    @Expose
    public String photoReference;
    @SerializedName("width")
    @Expose
    public int width;
}
