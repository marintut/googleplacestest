package com.example.googleplacesapptest.network.api_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchPlacesResult
{
    @SerializedName("html_attributions")
    @Expose
    private ArrayList<Object> htmlAttributions = null;
    @SerializedName("next_page_token")
    @Expose
    private String nextPageToken;
    @SerializedName("results")
    @Expose
    private ArrayList<Place> results = null;
    @SerializedName("status")
    @Expose
    private String status;


    public ArrayList<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public ArrayList<Place> getResults()
    {
        if (results == null)
            results = new ArrayList<>();

        return results;
    }

    public String getStatus() {
        return status;
    }
}
