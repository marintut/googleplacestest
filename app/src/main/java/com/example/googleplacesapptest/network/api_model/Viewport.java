package com.example.googleplacesapptest.network.api_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Viewport
{
    @SerializedName("northeast")
    @Expose
    public Northeast northeast;
    @SerializedName("southwest")
    @Expose
    public Southwest southwest;
}
