package com.example.googleplacesapptest.network;

import com.example.googleplacesapptest.C;
import com.example.googleplacesapptest.network.api_model.SearchPlacesResult;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PlaceAPI
{
    @GET(C.GOOGLE_PLACES_BASE_URL + "maps/api/place/nearbysearch/json")
    Observable<SearchPlacesResult> searchPlaces(@Query("location") String location,
                                                @Query("radius") int radius,
                                                @Query("keyword") String input,
                                                @Query("key") String key);

}
