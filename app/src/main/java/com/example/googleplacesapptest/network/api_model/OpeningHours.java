package com.example.googleplacesapptest.network.api_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpeningHours
{
    @SerializedName("open_now")
    @Expose
    public boolean openNow;
}
