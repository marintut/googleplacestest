package com.example.googleplacesapptest.network.api_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Geometry
{

    @SerializedName("location")
    @Expose
    public PlaceLocation location;
    @SerializedName("viewport")
    @Expose
    public Viewport viewport;
}
